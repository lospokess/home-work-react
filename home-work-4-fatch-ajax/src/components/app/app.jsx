import MyComponents from "../myComponents";
import "./app.css";

export const App = () => {
   return (
      <div className="App">
         <MyComponents />
      </div>
   )
}