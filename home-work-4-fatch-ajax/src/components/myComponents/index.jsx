import { Component } from "react";
import axios from 'axios';

import Cart from "../img/Add to cart.png";
import Star from "../svg/star.svg";

class MyComponents extends Component {
   state = {
      products: [],
   };

   componentDidMount() {
      axios.get('https://fakestoreapi.com/products')
         .then((response) => {
            const updatedProducts = response.data.map(item => {
               const modifiedDescription = item.description.replace(/\/(?! )|(,)(?! )/g, (slash, comma) => {
                  if (slash) {
                     return '/ '; // Додаємо пробіл після /
                  } else if (comma) {
                     return ', '; // Додаємо пробіл після ,
                  }
               });

               return {
                  ...item,
                  description: modifiedDescription
               };
            });

            this.setState({ products: updatedProducts });
         })
         .catch((error) => {
            console.error('Помилка при отриманні даних:', error);
         });
   }

   render() {
      const { products } = this.state;

      return (
         <div className="products__items">
            {products.map((item) => (
               <div key={item.id} className="box__product-item">
                  <div className="products__item" >
                     <a href="#" className="products__imge">
                        <img
                           src={item.image}
                           alt={item.title}
                        />
                     </a>
                     <a href="#" className="products__name">
                        {item.title}
                     </a>
                     <div className="product__description">
                        <div className="box__description">
                           <span>
                              {item.description}
                           </span>
                        </div>
                     </div>
                     <div className="product__rating rating">
                        <img src={Star} alt="" />
                        <span className="rate">{item.rating.rate}</span>
                        <div className="rating__star"></div>
                     </div>
                     <div className="product__price">
                        <span className="price-product">${item.price}</span>
                     </div>
                     <div className="box__add-cart">
                        <a href="#" className="product__add-tocart  add__tocart" >
                           <img src={Cart} alt="" />
                           ADD TO CART
                        </a>
                     </div>
                  </div>
               </div>
            ))}
         </div>
      );
   }
}

export default MyComponents;
