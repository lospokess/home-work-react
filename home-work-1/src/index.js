import React from 'react';
import ReactDOM from 'react-dom';
import './style.css';


function Table() {
   return (
      <table className='table'>
         <thead className='thead'>
            <tr className='thead__items'>
               <th>Назва зодіакального сузір'я</th>
               <th>Знак Сузір'я</th>
               <th>Термін дії зодіакального сузір'я</th>
            </tr>
         </thead>
         <tbody className='tbody'>
            <tr className='tbody__items'>
               <td>Овен</td>
               <td className='item'>(♈)</td>
               <td>21.03-20.04</td>
            </tr>
            <tr className='tbody__items'>
               <td>Телець</td>
               <td className='item'>(♉)</td>
               <td>21.04-21.05</td>
            </tr>
            <tr className='tbody__items'>
               <td>Близнюки</td>
               <td className='item'>(♊)</td>
               <td>22.05-21.06</td>
            </tr>
            <tr className='tbody__items'>
               <td>Рак</td>
               <td className='item'>(♋)</td>
               <td>22.06-22.07</td>
            </tr>
            <tr className='tbody__items'>
               <td>Лев</td>
               <td className='item'>(♌)</td>
               <td>23.07-21.08</td>
            </tr>
            <tr className='tbody__items'>
               <td>Діва</td>
               <td className='item'>(♍)</td>
               <td>22.08-23.09</td>
            </tr>
            <tr className='tbody__items'>
               <td>Терези</td>
               <td className='item'>(♎)</td>
               <td>24.09-23.10</td>
            </tr>
            <tr className='tbody__items'>
               <td>Скорпіон</td>
               <td className='item'>(♏)</td>
               <td>24.10-22.11</td>
            </tr>
            <tr className='tbody__items'>
               <td>Стрілець</td>
               <td className='item'>(♐)</td>
               <td>23.11-22.12</td>
            </tr>
            <tr className='tbody__items'>
               <td>Козоріг</td>
               <td className='item'>(♑)</td>
               <td>23.12-20.01</td>
            </tr>
            <tr className='tbody__items'>
               <td>Водолій</td>
               <td className='item'>(♒)</td>
               <td>21.01-19.02</td>
            </tr>
            <tr className='tbody__items'>
               <td>Риби</td>
               <td className='item'>(♓)</td>
               <td>20.02-20.03</td>
            </tr>
         </tbody>
      </table>
   )
}

ReactDOM.render(<Table></Table>, document.querySelector(".one"));

