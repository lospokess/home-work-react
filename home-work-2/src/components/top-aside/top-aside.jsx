import React from "react";
import "./top-aside.css"
function TopAside() {
   return (
      <div className="top-aside">
         <div className="box-person">
            <span className="arrow"></span>
            <div className="person-initial">AF</div>
            <div className="person-detail child">
               <div className="person-name">AnimatedFred</div>
               <div className="person-email">animated@demo.com</div>
            </div>
         </div>
      </div>
   )
}

export default TopAside;