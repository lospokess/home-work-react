import React from "react";

import "./middle-aside.css";

import dashboard from "../icon/dashboard.svg";
import revenue from "../icon/revenue.svg";
import notification from "../icon/notification.svg";
import analytics from "../icon/analytics.svg";
import inventory from "../icon/inventory.svg";

function MiddleAside() {
   return (
      <div className="middle-aside parent">
         <div className="box-search">
            <input type="text" placeholder="Search..." className="child-input" />
         </div>
         <nav className="nav">
            <ul className="nav__items">
               <li className="nav__item">
                  <a href="#" className="nav__link">
                     <img src={dashboard} alt="img" />
                     <div className="link-text child">Dashboard</div>
                  </a>
               </li>
               <li class="nav__item">
                     <a href="#" class="nav__link">
                        <img src={revenue} alt="imge" class="icon"/>
                        <div class="link-text child">Revenue</div>
                     </a>
                  </li>
                  <li class="nav__item">
                     <a href="#" class="nav__link">
                        <img src={notification} alt="imge" class="icon"/>
                        <div class="link-text child">Notifications</div>
                     </a>
                  </li>
                  <li class="nav__item">
                     <a href="#" class="nav__link">
                        <img src={analytics} alt="imge" class="icon"/>
                        <div class="link-text child">Analytics</div>
                     </a>
                  </li>
                  <li class="nav__item">
                     <a href="#" class="nav__link">
                        <img src={inventory} alt="imge" class="icon"/>
                        <div class="link-text child">Inventory</div>
                     </a>
                  </li>
            </ul>
         </nav>
      </div>
   )
}

export default MiddleAside;