import React from "react";

import "./bottom-aside.css";

import logout from "../icon/logout.svg"
import lightmode from "../icon/lightmode.svg"

function BottomAside() {
   return (
      <div className="bottom-aside">
         <div className="box-logout">
            <a href="#" class="nav__link">
               <img src={logout} alt="imge" class="icon" />
               <div class="link-text child">Logout</div>
            </a>
         </div>
         <div className="box-mode">
            <div className="name-mode child-log">
               <img src={lightmode} alt="imge" class="icon" />
               Light mode
            </div>
            <label className="check-label">
               <input type="checkbox" className="check-input" />
               <div className="check-div"></div>
            </label>
         </div>
      </div>
   )
}

export default BottomAside;