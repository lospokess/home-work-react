import React from "react";

import "./app.css";

import TopAside from "../top-aside/top-aside";
import MiddleAside from "../middle-aside/middle-aside";
import BottomAside from "../bottom-aside/bottom-aside";

const App = () => {
   return (
      <aside className="asaid parent">
         <TopAside></TopAside>
         <MiddleAside></MiddleAside>
         <BottomAside></BottomAside>
      </aside>
   )
}
/* <img src={notification} alt="" /> */
export default App;